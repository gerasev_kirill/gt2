# -*- coding: utf-8 -*-
import BaseHTTPServer
import SimpleHTTPServer
import pyjade
import os
import json
import sys

def exists_in_path(cmd):
  assert not os.path.dirname(cmd)

  extensions = os.environ.get("PATHEXT", "").split(os.pathsep)
  for directory in os.environ.get("PATH", "").split(os.pathsep):
    base = os.path.join(directory, cmd)
    options = [base] + [(base + ext) for ext in extensions]
    for filename in options:
      if os.path.exists(filename):
        return True
  return False



class RequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def get_jade_context(self):
        if not os.path.exists('./jade-' + PLATFORM + '.context'):
            raise Exception('No such file ./jade-' + PLATFORM + '.context!')
        with open('./jade-'+PLATFORM+'.context') as f:
            return json.load(f)

    def render_jade(self, orig_path):
        if os.path.isfile(orig_path) and 'views' not in orig_path:
            return orig_path
        jade_template_path = "%s.jade" % os.path.splitext(orig_path)[0]
        with open(jade_template_path) as f:
            src = f.read()
        parser = pyjade.parser.Parser(src)
        block = parser.parse()
        compiler = pyjade.ext.html.Compiler(block, pretty=True)
        compiler.global_context = self.get_jade_context()
        try:
            html = compiler.compile()
            with open(orig_path, 'w') as f:
                f.write(html)
        except:
            # pyjade не может собрать файлы в которых есть директива extends
            # FIXME: имеет смысл иногда заглядывать в
            # https://github.com/syrusakbary/pyjade/blob/master/pyjade/ext/html.py
            # и ожидать когда изменится блок
            #
            #    def visitExtends(self, node):
            #        raise pyjade.exceptions.CurrentlyNotSupported()
            #
            # 26.07.2016 - пока не реализовано.
            os.system(JADE + ' -p ' + jade_template_path + ' --out ' +
                      os.path.dirname(orig_path) +
                      ' --obj ./jade-'+PLATFORM+'.context' +
                      ' < ' + jade_template_path + ' > ' + orig_path)
        return orig_path


    def translate_path(self, path):
        ext = os.path.splitext(path)[-1]

        if ext.lower() == '.html':
            path = self.render_jade('.'+path)
        elif path == '/':
            path = self.render_jade('./views/index.html')
        elif not ext and '--as-angular-app' in sys.argv:
            p = path.split('/')
            if len(p)>1:
                print os.path.isfile('./views/'+p[1]+'.html')
                if len(p[1])>2 and os.path.isfile('./views/'+p[1]+'.html'):
                    path =  self.render_jade('./views/'+p[1]+'.html')
                else:
                    path = self.render_jade('./views/index.html')
            else:
                path = self.render_jade('./views/index.html')
        elif not ext:
            path = './views' + path + '.html'
            path = self.render_jade(path)

        return SimpleHTTPServer.SimpleHTTPRequestHandler.translate_path(self, path)

# NODE_ENV - переменная которая используется в loopback.js для определения
# где запущено приложение
PLATFORM = os.environ.get('NODE_ENV', 'dev')
# JADE - бинарник jade-препроцессора. т.к. jade переименовали в pug, добавил поддержку
# обоих.
if exists_in_path('jade'):
    JADE = 'jade'
elif exists_in_path('pug'):
    JADE = 'pug'
else:
    raise Exception("Ooops! Can't find jade or pug in PATH. Is it installed global with npm?")

port = os.environ.get('PORT', 8000)
server_address = ("", int(port))
httpd = BaseHTTPServer.HTTPServer(server_address, RequestHandler)
httpd.serve_forever()
