
angular.module 'OverrideAdminApp', [
    'ui.router',
    'gettext',
    'ui.cms.editable',
    'cms.models',
    'override.cms'
]


# этот модуль предназначен только для перезаписи параметров $bulk и $models
angular.module 'OverrideAfterAdminApp', [
    'cms.models'
]
