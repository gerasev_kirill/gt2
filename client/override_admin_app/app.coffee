angular.module('OverrideAfterAdminApp')
###
.constant 'adminConf', {
    visibleModels: [
        'CmsArticle',
        'CmsNews',
        'CmsKeyword',
        'CmsPage',
        'CmsPictureAlbum',
        'CmsSiteReview',
        'CmsUser',
        'CmsMailNotification',

        'CmsCategoryForArticle',
        'CmsCategoryForNews',
        'CmsCategoryForPage',
        'CmsCategoryForPictureAlbum'
    ],
    visiblePages: [
        'seo'
    ],
    navbarConf: {
        right:[
            {
                type: 'ui-sref',
                data: 'app.seo',
                title: {en:'SEO', ru:'SEO'},
                html: {pre: '<span class="fa fa-search"></span>'}
            },
            {
                type: 'href',
                data: "/",
                title: {en: 'Site', ru: 'Сайт'},
                attrs: {target: '_blank'},
                html: {pre: '<span class="fa fa-external-link"></span>'}
            },
            {
                type: 'directive',
                data: 'ui-lang-picker-for-navbar'
            },
            {
                type: 'directive',
                data: 'uice-logout-for-navbar'
            }
        ]
    }
}
###
angular.element(document).ready ()->
    angular.bootstrap(document, [
            'OverrideAdminApp',
            'AdminApp',
            'OverrideAfterAdminApp'
        ])
