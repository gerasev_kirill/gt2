# раскомментить этот блок если нужно переопределение директив.
# переопределять только те директивы, которые действительно нужны!
###
angular.module('override.cms')





.config ($directiveOverrideProvider)->
    $directiveOverrideProvider
        .override('uicvArticle', {
            templateUrl: '/@@__SOURCE_PATH__/uicvPage.html'
        })
        .override('uicvNews', {
            templateUrl: '/@@__SOURCE_PATH__/uicvNews.html'
        })
        .override('uicvPage', {
            templateUrl: '/@@__SOURCE_PATH__/uicvPage.html'
        })
        .override('uicvSiteReview', {
            templateUrl: '/@@__SOURCE_PATH__/uicvSiteReview.html'
        })
###
