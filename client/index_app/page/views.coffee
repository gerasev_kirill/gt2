angular.module('IndexApp')




.controller 'PageView', ($scope, $state, $stateParams, $models) ->
    identifier = decodeURIComponent($stateParams.identifier)
    identifier = decodeURIComponent(identifier)
    $stateParams.identifier = identifier
    $state.transitionTo('app.page', $stateParams, {notify: false})

    $scope.item = $models.CmsPage.item





.config ($stateProvider)->
    $stateProvider
         .state('app.page', {
             url: '/page/:identifier',
             controller: 'PageView',
             templateUrl: '/@@__SOURCE_PATH__/View.html',
             $bulk: (defaults, toParams, $models, bulk)->
                 identifier = decodeURIComponent(toParams.identifier)
                 identifier = decodeURIComponent(identifier)
                 {
                     CmsPage: {
                         findByIdentifier:{
                             identifier: identifier,
                             $replaceObj: 'item'
                         }
                     }
                 }
             resolve:{
                $title: ()-> 'Page'
             }
         })
