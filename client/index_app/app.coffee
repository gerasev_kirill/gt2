angular.module('IndexApp')




.run ($langPicker)->
    $langPicker.setLanguageList({
        en: "English",
        ru: "Русский",
        ua: "Українська",
        cz: "Čeština",
        de: "Deutsch",
    })
    $langPicker.detectLanguage()




angular.element(document).ready \
    ()-> angular.bootstrap(document, ['IndexApp'])
