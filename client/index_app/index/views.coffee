angular.module('IndexApp')




.controller 'IndexView', ($scope, $state, $stateParams, $injector) ->
    if $injector.has('$cms')
        $cms = $injector.get('$cms')
        $scope.$cms = $cms






.config ($stateProvider)->
    $stateProvider
         .state('app.index', {
             url: '/',
             controller: 'IndexView',
             templateUrl: '/@@__SOURCE_PATH__/View.html',
             resolve:{
                $title: ()-> 'Index'
             }
         })
