angular.module('IndexApp')




.config ($locationProvider, $stateProvider, $urlRouterProvider, cfpLoadingBarProvider, $bulkProvider)->
    cfpLoadingBarProvider.includeSpinner = false
    $locationProvider.html5Mode(true)
    #$locationProvider.hashPrefix('!')

    # see http://stackoverflow.com/questions/16793724/otherwise-on-stateprovider
    $urlRouterProvider.otherwise  ($injector)->
        $state = $injector.get('$state')
        $state.go('app.index')

    $stateProvider.state('app', {
        abstract: true,
        url: '/{lang:(?:ru|ua|en|fr|de|cz)}',
        template: '<ui-view class="ui-view-animation"/>'
    })


    $bulkProvider.defaultOperations = {
       CmsSettings:{findOne:true}
    }

