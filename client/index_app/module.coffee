angular.module 'IndexApp', [
        'ngAnimate',
        'gettext',
        'angular-loading-bar',
        'duScroll',
        'ui.bootstrap',
        'ui.router',
        'ui.router.title',
        'ui.gettext.langPicker',
        'ui.cms',
        'cms.models',
        'helpers',
        'override.cms'
]
