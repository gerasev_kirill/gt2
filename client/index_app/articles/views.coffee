angular.module 'IndexApp'










.controller 'ArticlesDetailsView', ($scope,  $stateParams, $models) ->
    $scope.id = $stateParams.id
    $scope.item = $models.CmsArticle.item










.config ($stateProvider)->
    $stateProvider
        .state('app.articles', {
            url: '/articles',
            abstract: true,
            template: '<ui-view class="ui-view-animation"/>',
        })
        .state('app.articles.details', {
            url: '/id/:id',
            controller: 'ArticlesDetailsView',
            templateUrl: '/@@__SOURCE_PATH__/DetailsView.html',
            $bulk: (defaults, toParams, $models, bulk)->
                {
                    CmsArticle: {
                        findOne:{
                            filter: {where:{id:toParams.id}},
                            $mergeWithArray: 'items',
                            $replaceObj: 'item'
                        }
                    }
                }
            resolve:{
               $title: ()-> 'Articles'
            }
        })
