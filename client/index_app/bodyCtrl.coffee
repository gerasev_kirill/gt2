angular.module('IndexApp')



.controller 'BodyCtrl',
    ($scope, $document, $state, $rootScope, $models, $cms, $bulk)->
        
        $scope.$models = $models
        $scope.$cms = $cms
        # вместо этой переменной можно использовать $bulk.ready,
        # тогда крутилка будет cпрятана после того, как $bulk загрузит все с
        # сервера.
        $scope.ready = true

        $scope.navbarConf = {
            right: [
                {type: 'ui-sref', data: 'app.index', title:{en: "Index", ru: "Главная", ua: "Головна", cz: "Hlavní" }},
                {type: 'href', data: 'google.com', title:{en: "Some link with auto nofollow", ru: "Ссылка с auto nofollow", ua: "Лiнк з auto nofollow", cz: "Link + auto nofollow"}, attrs:{target: '_blank'}},
                {type: 'directive', data: 'ui-lang-picker-for-navbar'}
            ]
        }
