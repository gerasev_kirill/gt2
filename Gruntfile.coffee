gruntHelpers = require('lb-cms-helpers').gruntHelpers


index_app = [
    'helpers_app',
    'override_cms_app',
    'index_app'
]
admin_app = [
    'override_cms_app',
    'override_admin_app'
]


module.exports = (grunt)->
    context = grunt.file.readJSON('./jade-prod.context')
    CMS_DB_BUCKET = context.CMS_DB_BUCKET

    grunt.initConfig {
        pkg: grunt.file.readJSON('package.json')
        replace:
            options:{},
            files:{
                expand: true,
                cwd: 'client',
                src: ['**/*.js', '**/*.html'],
                dest: 'client',
            }
        tags:
            index: {
                options: {
                    scriptTemplate: '<script src="/{{ path }}"></script>',
                    linkTemplate: '<link href="/{{ path }}"/>',
                    absolutePath: true
                },
                src: gruntHelpers.generateFilePattern(index_app, '.js'),
                dest: 'views/index.jade'
            },
            admin: {
                options: {
                    scriptTemplate: '<script src="/{{ path }}"></script>',
                    linkTemplate: '<link href="/{{ path }}"/>',
                    absolutePath: true
                },
                src: gruntHelpers.generateFilePattern(admin_app, '.js'),
                dest: 'views/admin.jade'
            }
        less_imports:
            index:{
                options:{},
                src: gruntHelpers.generateFilePattern(index_app, '.less'),
                dest: 'client/index_auto_imports.less'
            },
            admin:{
                options:{},
                src: gruntHelpers.generateFilePattern(admin_app, '.less'),
                dest: 'client/admin_auto_imports.less'
            }
        watch:
            js_html:{
                files: ['client/*_app/**/*.js', 'client/*_app/**/*.html' ],
                tasks: ['replace', 'tags']
            },
            css_index:{
                files: [
                        'client/bootstrap_extra.less', 'client/theme.less',
                        'client/variables.less', 'client/site.less'
                    ].concat(
                        gruntHelpers.generateFilePattern(index_app, '.less')
                    )
                tasks: ['less_imports', 'less:prod_index']
            },
            css_admin:{
                files: [
                        'client/bootstrap_extra.less', 'client/theme.less',
                        'client/variables.less', 'client/site.less',
                        'client/admin.less'
                    ].concat(
                        gruntHelpers.generateFilePattern(admin_app, '.less')
                    )
                tasks: ['less_imports', 'less:prod_admin']
            }
        coffee:
            glob_to_multiple: {
                expand: true,
                cwd: 'client',
                src: ['**/*.coffee'],
                dest: 'client',
                ext: '.js'
            }
        # pug == jade templates
        pug:
            views:{
                options:{
                    data: grunt.file.readJSON('./jade-dev.context')
                }
                files: [{
                    expand: true,
                    cwd: 'views',
                    src: ['**/*.jade', '**/*.pug'],
                    dest: 'views',
                    ext: '.html'
                }]
            },
            views_prod:{
                options:{
                    data: grunt.file.readJSON('./jade-prod.context')
                }
                files: [{
                    expand: true,
                    cwd: 'views',
                    src: ['**/*.jade', '**/*.pug'],
                    dest: 'views',
                    ext: '.html'
                }]
            },
            client:{
                files: [{
                    expand: true,
                    cwd: 'client',
                    src: ['**/*.jade', '**/*.pug'],
                    dest: 'client',
                    ext: '.html'
                }]
            },
        concat:
            options:{
                separator:';\n'
            },
            index_app:{
                src: gruntHelpers.generateFilePattern(index_app, '.js')
                dest:'client/index_app.js'
            },
            bower_js_libs:{
                src: gruntHelpers.generateBowerLibsList(
                    'views/base/index.jade',
                    grunt.file.readJSON('./bower.index.json')
                ),
                dest: 'client/bower_libs.min.js'
            },
            admin_app:{
                src: gruntHelpers.generateFilePattern(admin_app, '.js')
                dest:'client/override_admin_app.js'
            },
            bower_admin_js_libs:{
                src: gruntHelpers.generateBowerLibsList(
                    'views/base/admin.jade',
                    grunt.file.readJSON('./bower.admin.json')
                ),
                dest: 'client/bower_libs.admin.min.js'
            }
        less:
            prod_index:{
                options:{compress:true},
                files:{
                    'client/site.min.css':'client/site.less',
                }
            },
            prod_admin:{
                options: {compress:true},
                files:{
                    'client/admin.min.css':'client/admin.less',
                }
            }
            preloader:{
                options: {compress: true},
                files:{
                    'client/preloader.css': 'client/preloader.less'
                }
            }
        wiredep:
            index: {
                src: [ 'views/base/index.jade' ],
                options: {
                    cwd: './',
                    ignorePath: '..',
                    #ignorePath: '../bower_components',
                    dependencies: true,
                    devDependencies: false,
                    bowerJson: grunt.file.readJSON('./bower.index.json')
                }
            },
            admin: {
                src: [ 'views/base/admin.jade' ],
                options: {
                    cwd: './',
                    ignorePath: '..',
                    #ignorePath: '../bower_components',
                    dependencies: true,
                    devDependencies: false,
                    bowerJson: grunt.file.readJSON('./bower.admin.json')
                }
            }
        nggettext_extract:
            pot: {
                files: {
                    'po/template.pot': ['views/**/*.html', 'client/**/*.html']
                }
            }
        angular_template_inline_js:
            options:{
                basePath: __dirname
            },
            files:{
                cwd: 'client',
                expand: true,
                src: ['*_app.js'],
                dest: 'client'
            }
        ngAnnotate:
            files:{
                cwd: 'client',
                expand: true,
                src: ['*_app.js']
                dest: 'client'
            }
        uglify:
            all:{
                files:{
                    'client/index_app.min.js':['client/index_app.js'],
                    'client/bower_libs.min.js':['client/bower_libs.min.js'],
                    'client/override_admin_app.min.js':['client/override_admin_app.js'],
                    'client/bower_libs.admin.min.js':['client/bower_libs.admin.min.js']
                }
            }
        prepare_for_gcloud:
            options:{
                bucket: CMS_DB_BUCKET
                white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
            }
            files:{
                cwd: '.'
                src: ['client/**/*.html', 'views/*.html']
            }
    }

    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.loadNpmTasks 'grunt-script-link-tags'
    grunt.loadNpmTasks 'grunt-replace'
    grunt.loadNpmTasks 'grunt-wiredep'
    grunt.loadNpmTasks 'grunt-contrib-concat'
    grunt.loadNpmTasks 'grunt-contrib-less'
    grunt.loadNpmTasks 'grunt-simple-watch'
    grunt.loadNpmTasks 'grunt-less-imports'
    grunt.loadNpmTasks 'grunt-contrib-pug'
    grunt.loadNpmTasks 'grunt-angular-template-inline-js'
    grunt.loadNpmTasks 'grunt-contrib-uglify'
    grunt.loadNpmTasks 'grunt-ng-annotate'
    grunt.loadNpmTasks 'grunt-angular-gettext'

    grunt.loadNpmTasks 'lb-cms-helpers/lib'




    build_tasks = [
        'coffee'
        'tags'
        'less:preloader'
        'pug:views_prod'
        'pug:client'

        'less_imports'
        'less'
        'replace'
        'concat:index_app'
        'concat:admin_app'
        'ngAnnotate'

        'concat:bower_js_libs'
        'concat:bower_admin_js_libs'
    ]

    grunt.registerTask 'build', build_tasks.concat([
        'angular_template_inline_js'
        'uglify:all'
    ])

    grunt.registerTask 'build-prod', build_tasks.concat([
        'nggettext_extract'
        'prepare_for_gcloud'
        'angular_template_inline_js'
        'uglify:all'
    ])

    grunt.registerTask 'pot', [
        'pug:views_prod',
        'pug:client',
        'nggettext_extract'
    ]

    grunt.registerTask 'init', [
        'wiredep',
        'pot',
        'build'
    ]

    grunt.registerTask 'default', 'simple-watch'
