# settings for python, django
MANAGE=./manage.py
SETTINGS=www_site.settings
PYTHON=python2

# GOOGLE CLOUD settings
PROGECT_ID = phrasal-edition-136623
GCE_ZONE = europe-west1-d

# lb-cms settings
CMS_DB_BUCKET = gt2

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
.SILENT: install_virtualenv2


install_virtualenv2:
	( \
		virtualenv .virtualenv/p2;\
		chmod +x .virtualenv/p2/bin/activate;\
		rm ./ap2 || true;\
		ln -s .virtualenv/p2/bin/activate ap2 || true;\
		bash ./scripts/pip_install.sh;\
	)

run:
	env NODE_ENV=dev bash ./scripts/run_server.sh

run-prod:
	env NODE_ENV=prod bash ./scripts/run_server.sh

run-mixed:
	env NODE_ENV=mixed bash ./scripts/run_server.sh




build-prod:
	grunt build-prod

sync-deploy-dir:
	rm -fr ./.__deploy__ || true
	( \
		rsync -zarv --prune-empty-dirs --include "*.min.js" --include "*/" --exclude "*" ./client ./.__deploy__ ;\
		rsync -zarv --prune-empty-dirs --include "*.json" --include "*/" --exclude "*" ./client ./.__deploy__ ;\
		rsync -zarv --prune-empty-dirs --include "*.min.css" --include "*/" --exclude "*" ./client ./.__deploy__ ;\
		rsync -zarv --prune-empty-dirs --include "*.png" --include "*.jpg" --include "*.gif" --include "*/" --exclude "*" ./client ./.__deploy__ ;\
		rsync -avc --prune-empty-dirs --include "*.html" --include "views/" --exclude "*" ./views ./.__deploy__ ;\
	)
	cp ./.__deploy__/views/index.html ./.__deploy__/index.html

deploy:
	gcloud compute --project $(PROGECT_ID) copy-files ./.__deploy__/* node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html/ --zone $(GCE_ZONE)
	gcloud compute --project $(PROGECT_ID) copy-files ./_htaccess node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html/.htaccess --zone $(GCE_ZONE)

build-prod-and-deploy: build-prod sync-deploy-dir deploy

build-prod-and-deploy-to-googlestorage:
	grunt build-prod-and-deploy-to-googlestorage
	gcloud compute --project $(PROGECT_ID) copy-files ./views/*.html node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html/views --zone $(GCE_ZONE)
	gcloud compute --project $(PROGECT_ID) copy-files ./views/index.html node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html --zone $(GCE_ZONE)
	gcloud compute --project $(PROGECT_ID) copy-files ./client/languages/*.json node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html/client/languages --zone $(GCE_ZONE)
	gcloud compute --project $(PROGECT_ID) copy-files ./_htaccess node-user@shared-hosting--git:/var/www/$(CMS_DB_BUCKET)/public_html/.htaccess --zone $(GCE_ZONE)
